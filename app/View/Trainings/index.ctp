<div id="my_carousel" class="carousel slide" data-ride="false">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="img/training.jpg" alt="bgimg" style="width:100%;margin-top:70px;" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/training1.jpg" alt="bgimg" style="width:100%;margin-top:70px;" alt="Second slide">

    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/training3.jpg" alt="bgimg" style="width:100%;margin-top:70px;" alt="Third slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/training2.jpg" alt="bgimg" style="width:100%;margin-top:70px;" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#my_carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#my_carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <ol class="carousel-indicators">
    <li data-target="#my_carousel" data-slide-to="0" class="active"></li>
    <li data-target="#my_carousel" data-slide-to="1"></li>
    <li data-target="#my_carousel" data-slide-to="2"></li>
    <li data-target="#my_carousel" data-slide-to="3"></li>
  </ol>
</div>
<div class="container opening">
  <?php echo $this->Session->flash('positive') ?>
  <h3 style="margin-bottom:30px;">Training Programs</h3>
  <p>
    We are a contract based service provider based in the beautiful city of Dehradun, India. We laid our foundation stone five years back with the aim of not only meeting our client’s requirements but offering them some unique breakthrough innovative ideas which would benefit their businesses in the long run. We have a very experienced team of developers and managers who have all the capabilities to render the most professional IT services to various business niches including but not limited to eCommerce, home improvement, healthcare and travel.
    invites all the fresher students and other needy persons to join our job oriented web designing and web development Trainning. Our professional team will guide you effectively with live projects.
    <ul class="fa-ul ">
      <li><i class="fa-li fa fa-check"></i>Training on live projects.</li>
      <li><i class="fa-li fa fa-check"></i>App desinning & development.</li>
      <li><i class="fa-li fa fa-check"></i>No theory, only practical programming knowledge.</li>
      <li><i class="fa-li fa fa-check"></i>Web designning & development.</li>
  </p>
  </ul>
  <div class="row">
    <div class="col-lg-6">
      <div class="job card">
        <div class="job_title">
          <h5>Web Designning</h5>
        </div>
        <div class="job_description">
          <div class="benifits_image">
            <ul class="fa-ul ">
              <li><i class="fa-li fa fa-check"></i> <a href="#">Html5, CSS3, XHTML</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#" target="_blank">Dreamweaver, Adobe Photoshop, Fireworks, Flash</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#" target="_blank">Bootstrap Framework</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#">Material Designing</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="job card">
        <div class="job_title">
          <h5>Web Development</h5>
        </div>
        <div class="job_description">
          <div class="benifits_image">
            <ul class="fa-ul ">
              <li><i class="fa-li fa fa-check"></i> <a href="#">Core Php,MySQL</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#" target="_blank">Laravel, Cake Php, Angular</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#" target="_blank">Wordpress, Zoomla</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#">Material Designing</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="job card">
        <div class="job_title">
          <h5>App Designning</h5>
        </div>
        <div class="job_description">
          <div class="benifits_image">
            <ul class="fa-ul ">
              <li><i class="fa-li fa fa-check"></i> <a href="#">Html5, CSS3, XHTML</a></li>

              <li><i class="fa-li fa fa-check"></i> <a href="#" target="_blank">Ionic Framework</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#">Material Designing</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>


    <div class="col-lg-6">
      <div class="job card">
        <div class="job_title">
          <h5>App Development</h5>
        </div>
        <div class="job_description">
          <div class="benifits_image">
            <ul class="fa-ul ">
              <li><i class="fa-li fa fa-check"></i> <a href="#">Angular,Ionic,React Native, Java</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#">React Native, Java</a></li>
              <li><i class="fa-li fa fa-check"></i> <a href="#" target="_blank">Hybrid App, Native App, Node.js</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg">
      <div class="container">

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="card">
              <!-- <img src="img/why-us-1.jpg" class="card-img-top" alt="..."> -->
              <?php echo $this->Html->image('why-us-1.jpg', array('alt' => 'processimg22', 'width' => '100%')) ?>
              <div class="card-icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="">Proffesional Team</a></h5>
                <p class="card-text">We have a proffesional team of smart & extremely talented peoples which are responsible for our fabulous work. </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="card">
              <!-- <img src="img/why-us-2.png" class="card-img-top" alt="..."> -->
              <?php echo $this->Html->image('why-us-2.png', array('alt' => 'processimg1', 'width' => '100%')) ?>
              <div class="card-icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="">Latest Technologies</a></h5>
                <p class="card-text">We are used the latest Technologies, such as Angular, React navive, Node js, Laravel, Wordpress, MySQL, Cake php,Html5, Css3, Jquery etc., for web & app development. </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="card">
              <!-- <img src="img/why-us-3.jpg" class="card-img-top" alt="..."> -->
              <?php echo $this->Html->image('why-us-3.jpg', array('alt' => 'processimg', 'width' => '100%')) ?>
              <div class="card-icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="">Friendly Environment</a></h5>
                <p class="card-text">The easiest thing you to curate a friendly work environment is promote respect between everyone who with us. Oue open communication leads to a happier work environment in our workspace.</p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->
<div class="technology">
  <h3>Technology We Use</h3>
  <!-- <img  src="img/tech.png" alt="bgimg" style="width:100%;"> -->
  <?php echo $this->Html->image('tech.png', array('alt' => 'processimg', 'width' => '100%')) ?>
</div>



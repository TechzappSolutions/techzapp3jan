<div class="careerparallax">
  <img src="img/career.jpg" alt="bgimg" width="100%">
</div>




<div class="container opening">
  <?php echo $this->Session->flash('positive') ?>
  <h3>Current Openings</h3>

  </p>
  <div class="row">

    <div class="col-lg-6">

      <div class="job card">
        <div class="job_title">
          <h5>Php Developer</h5>
        </div>
        <div class="job_description">
          <P><strong>Requirement :</strong> Must have at-least 1.5 years of experience in PHP and MySQL.
            Good knowledge of Laravel 5.x/ CakePHP/ CodeIgniter.

          </P>
          <p><strong>Number of vcancies :</strong> 2</p>
          <p><strong>Job Description :</strong></p>
          <ul>
          <li>DevOps knowledge preferred.</li>
            <li>Knowledge of Linux and GIT is preferred.</li>
            <li>Qualification : Graduate in any stream</li>
          </ul>
          <p><strong>Experience :</strong> 1.5 to 3 year</p>
          <p><strong>Job Type : </strong>Full-time</p><br>

          <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Apply</button>
        </div>
      </div>
    </div>

    <div class="col-lg-6">

      <div class="job card">
        <div class="job_title">
          <h5>Full Stack Developer</h5>
        </div>
        <div class="job_description">
          <P><strong>Requirement :</strong> Should have at-least 1.5 years of experience.
            Good knowledge of Laravel 5.x/ CakePHP, Angular, ReactJS, Front End.
       
     </P>
          <p><strong>Number of vcancies :</strong> 2</p>
          <p><strong>Job Description :</strong></p>
          <ul>
            <li>DevOps knowledge preferred.</li>
            <li>Knowledge of Linux and GIT is preferred.</li>
            <li>Qualification : Graduate in any stream</li>
          </ul>
          <p><strong>Experience :</strong> 1.5 to 3 year</p>
          <p><strong>Job Type : </strong>Full-time</p><br>

          <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Apply</button>
        </div>
      </div>
    </div>



  </div>
</div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Upload Your Resume</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

        <?php echo $this->Form->create('Job', array('url' => array('controller' => 'Careers', 'action' => 'jobapply'), 'enctype' => 'multipart/form-data')); ?>
        <div class="form-group">
          <?php echo $this->Form->input('fullname', array('type' => 'text',  'class' => 'form-control',  'div' => false, 'label' => 'FullName<span class="required">*</span>')); ?>
        </div>
        <div class="form-group">
          <?php echo $this->Form->input('job_position', array('type' => 'text',  'class' => 'form-control',  'div' => false, 'label' => 'Job Position<span class="required">*</span>')); ?>
        </div>
        <div class="form-group">
          <?php echo $this->Form->input('resume', array('type' => 'file', 'id' => 'file', 'accept' => '.doc, .docx,.pdf')); ?>
        </div>
        <div class="form-group">
          <?php echo $this->Form->input('message', array('type' => 'textarea', 'cols' => '40', 'rows' => '5',  'class' => 'form-control', 'id' => 'message', 'div' => false, 'label' => 'Message<span class="required">*</span>')); ?>
        </div>

        <div class="form-group">
          <?Php echo $this->Form->button('Submit', array('type' => 'submit', 'class' => 'btn btn-danger')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
      </div>

    </div>
  </div>
</div>
<script>
  $("#JobIndexForm").validate({
    rules: {
      "data[Job][fullname]": {
        required: true,
        maxlength: 20,
      },
      "data[Job][job_position]": {
        required: true,
        maxlength: 40,
      },
      "data[Job][message]": {
        required: true,
      },
    },
    messages: {
      "data[Job][fullname]": {
        required: "Please enter fullname.",
        minlength: 'Minimum length should be greater than 25 characters',
      },
      "data[Job][job_position]": {
        required: "Please enter position which yo want to apply.",
        maxlength: 'Maxlength must be 40 digits',
      },
      "data[Job][message]": {
        required: "Please enter your message.",
      },
    },
  });
</script>
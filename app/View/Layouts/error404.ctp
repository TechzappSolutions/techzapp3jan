<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title; ?></title>
	<link rel="shortcut icon" href="/icon.ico" type="image/x-icon">
	<link rel="icon" href="/icon.ico" type="image/x-icon">
	<?php
	echo $this->Html->css('cake.generic');
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	echo $this->fetch('script');
	echo $this->Html->css('bootstrap');
	echo $this->Html->css('style.css');
	echo $this->Html->css('bootstrap.min');
	?>
</head>
<body>
	<?php echo $this->fetch('content');?>
</body>
</html>
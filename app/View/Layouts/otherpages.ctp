<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="theme-color" content="#e91e63;">
	<meta name="description" content="Techzapp Solution">
	<meta name="keywords" content="HTML,CSS,JavaScript,cake php">
	<meta name="author" content="Techzapp solutions">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title; ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<!-- <link rel="icon" href="/favicon.ico" type="image/x-icon"> -->
	<?php
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	echo $this->Html->css('bootstrap.min.css');
	echo $this->Html->css('font-awesome.min.css');
	echo $this->Html->css('animate.min.css');
	echo $this->Html->css('ionicons.min.css');
	echo $this->Html->css('magnific-popup.css');
	echo $this->Html->css('owl.carousel.min.css');
	echo $this->Html->css('lightbox.min.css');
	echo $this->Html->css('style.css');
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('jquery-migrate.min.js');
	echo $this->Html->script('bootstrap.bundle.min.js');
	echo $this->Html->script('hoverIntent.js');
	echo $this->Html->script('superfish.min.js');
	echo $this->Html->script('wow.min.js');
	echo $this->Html->script('waypoints.min.js');
	echo $this->Html->script('counterup.min.js');
	echo $this->Html->script('owl.carousel.min.js');
	echo $this->Html->script('isotope.pkgd.min.js');
	echo $this->Html->script('lightbox.min.js');
	echo $this->Html->script('jquery.touchSwipe.min.js');
	echo $this->Html->script('float-panel.js');
	echo $this->Html->script('main.js');
	echo $this->Html->script('jquery-validate');
	echo $this->Html->script('delighters.js');
	echo $this->Html->script('magnific-popup.min.js');
	?>
</head>

<body>
<div class="loader"></div>

	<?php echo $this->element('navbar');
	echo $this->fetch('content');
	echo $this->element('footer'); ?>

</body>
<script type="text/javascript">
$(window).load(function() {
	// setTimeout(function () {
    $(".loader").fadeOut("slow");
// }, 10000);
});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</html>
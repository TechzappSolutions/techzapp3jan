<header id="intro">
  <!-- <button class="openbtn" onclick="openNav()"><span style="font-weight: bold;font-size:18px;">☰</span> </button>   -->
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="img/vid.mp4" type="video/mp4">
  </video>
  <div class="col-lg-12 headertext">
    <h2><span style="color:#ea2163;">Techzapp</span> Bring To You A World Of Creativity and Productivity</h2><br>
    <h2><span style="color:#e91e63;">Website Designing & Development</span>
    </h2>
  </div>

</header>
<section id="section03" class="srcolldown">
  <a href="#about"><span></span></a>
</section>

<main id="main">
  <!--==========================
About Us Section
============================-->
  <section id="about">

    <div class="about container slideanim">
      <h3>About Us</h3>
      <div class="row">
        <div class="col-lg-5">
          <?php echo $this->Html->image('about.webp', array('alt' => 'img', 'width' => '100%')) ?>
        </div>
        <div class="col-lg-7">
          <p>We are a contract based service provider based in the beautiful city of Dehradun, India. We
            laid our foundation stone five years back with the aim of not only meeting our client’s
            requirements but offering them some unique breakthrough innovative ideas which would benefit
            their businesses in the long run. We have a very experienced team of developers and managers
            who have all the capabilities to render the most professional IT services to various
            business niches including but not limited to eCommerce, home improvement, healthcare and
            travel.
          </p>
        </div>
      </div>
    </div>






    <div class="container" style="  margin-top: 50px; margin-bottom: 30px;">
      <div class="nav tab nav-tabs nav-fill" id="nav-tab" role="tablist">
        <button class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Our Mission</button>
        <button class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Our Essence</button>
        <button class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Our Promise</button>
        <button class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">Our Vibe</button>
        <div>

          <div class="tab-content" id="nav-tabContent">

            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <p>In the fast track IT industry, TechZapp believes in sticking to the basics to deliver the best
                results for our clientele. With plenty of experience of the market, our professionals follow the
                fundamental path of firstly understanding the needs of our customers and only then proceeding with the
                project. For the same, we have set up a simple yet effective mechanism which surely would impress you.
                Like lots of web businesses, we started as one enthusiastic individual (our founder) working in his
                spare room. This was in 2017 and he was joined by Sachin negi then Abhishek raj in 2017, and it was
                around this time that the company name was Techzapp. We’ve always been, first and foremost, a web
                company; we didn’t morph out of a design studio or IT support organisation. We really do love the web
                and we love helping our clients do amazing things with it.</p>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
              <p>TechZapp believes technology is the most essential element for every company’s growth, no matter what
                business it does. Being such an important factor, the industry is consistently evolving and our team
                of tech enthusiasts have enough knowledge and experience of working on the prominent technologies to
                excel in the field. We might not cover all the technologies of today’s age but we deliver wonderfully
                hi-tech results in what we do</p>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
              <p>Like most agencies we’ve seen a lot of change in our industry over the last 5 years. In the Spring of
                2015 we decided to formalise something we’ve always done, which is taking a business focused approach
                to all our work, looking at the why as well as the what and challenging our clients when it’s
                appropriate. We put these together and became an inbound marketing agency. This move saw us invest
                heavily in software and training, gain our HubSpot Certification and make our first acquisition, Debut
                Marketing. These steps have enhanced our ability to advise our clients how to use their websites to
                achieve their business objectives and implement end-to-end online marketing campaigns to pull in more
                leads. Inbound marketing is a marketing methodology that’s now gaining some real currency in the
                Uttrakhand and it's given us a solid base to build on for the future. For a chat about how we can help
                your business to bring you the leads you need, simply get in touch.</p>
            </div>
            <div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
              <p>Apart from delivering best quality services, we have also maintained a reputation of never
                missing the deadline for delivering any milestone of the projects. During the past few years, we
                have established few very strong bonds with our clients which give us confidence of keep moving
                ahead in the industry.</p>
            </div>
          </div>
        </div>
      </div>








  </section>

  <div class="provide container slideanim">
    <h3 class="right">What We Provide</h3>
    <div class="row ">
      <p>We like to fully understand our clients' needs and desires before proceeding with the designing and
        development tasks. Our tech savvy professionals have complete knowledge of latest technologies
        available in the industry today and they use their expertise to get complete customer satisfaction.<br>
        As a dedicated IT company, we deliver the following services to our customers: </p>
      <ul data-delighter>
        <li class="text_bold">Web Designing </li>
        <li class="text_bold">Web Development </li>
        <li class="text_bold">Seo & Digital Marketing </li>
        <li class="text_bold">IT Consulting </li>
        <li class="text_bold">Ecommerce Solutions </li>
        <li class="text_bold">Mobile Development </li>
        <li class="text_bold">Software Designing Development </li>
        <li class="text_bold">Content Management System </li>
      </ul>
      <p>Our technical staff owns all the experience required in designing, development and maintaining the
        requirements of every type of websites. We are just a phone call away all the time in case you have
        any sort of technical issues with your website or mobile application. So, after choosing us, you can
        rest assured that your project is in the right hands and will be executed as per your needs and
        desires. <br>
        So, to outride your challengers in your industry, just give us a call or fill up the Contact Us enquiry
        enquiry form and tell us your requirements. We can assure you, it would be a pleasent experience
        doing business with us.
      </p>
    </div>
  </div>
  <!-- #about -->
  <section class="service " id="service">
    <h3>Our Services</h3>
    <div class="container ">
      <p>TechZapp believes technology is the most essential element for every company's growth, no matter
        what business it does. Being such an important factor, the industry is consistently evolving and our
        team of tech enthusiasts have enough knowledge and experience of working on the prominent
        technologies to excel in the field. We might not cover all the technologies of today's age but we
        deliver wonderfully hi-tech results in what we do. </p>
      <div class="row mbr-justify-content-center">
        <div class="col-lg-6 mbr-col-md-10 slideanim">
          <div class="wrap1">
            <div class="ico-wrap">
              <span class="mbr-iconfont fa-desktop fa"></span>
            </div>
            <div class="text-wrap vcenter">
              <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5">Web Design &
                Development</h2>
              <p class="mbr-fonts-style text1 mbr-text display-6">We design, develop and manage
                pixel-perfect, fully responsive websites that not only look amazing but function
                well too.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mbr-col-md-10 slideanim">
          <div class="wrap2">
            <div class="ico-wrap">
              <span class="mbr-iconfont fa-calendar fa"></span>
            </div>
            <div class="text-wrap vcenter">
              <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5">Digital Marketing
              </h2>
              <p class="mbr-fonts-style text1 mbr-text display-6">We are a Full Service Digital
                Marketing Agency because no online marketing strategy exists in vacuum.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mbr-col-md-10 slideanim">
          <div class="wrap3">
            <div class="ico-wrap">
              <span class="mbr-iconfont fa-globe fa"></span>
            </div>
            <div class="text-wrap vcenter">
              <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5">Domain Registration
              </h2>
              <p class="mbr-fonts-style text1 mbr-text display-6">It is process of registering Domain name that
                identifies one or more with the name that is easier to remember. We work towards securing the
                guarantee web site domain for your site.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mbr-col-md-10 slideanim">
          <div class="wrap4">
            <div class="ico-wrap">
              <span class="mbr-iconfont fa-android fa"></span>
            </div>
            <div class="text-wrap vcenter">
              <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5">App Design &
                Development</h2>
              <p class="mbr-fonts-style text1 mbr-text display-6">We design, develop and manage
                pixel-perfect, fully responsive websites that not only look amazing but function
                well too.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mbr-col-md-10 slideanim">
          <div class="wrap5">
            <div class="ico-wrap">
              <span class="mbr-iconfont fa-folder-open-o fa"></span>
            </div>
            <div class="text-wrap vcenter">
              <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5">Open Source </h2>
              <p class="mbr-fonts-style text1 mbr-text display-6">We mainly work on Open-Source Software (OSS) which surely is the most gigantic industry in
                today's tech age. Our team of developers have exceptional knowledge and experience of working
                on the technology . </p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mbr-col-md-10 slideanim">
          <div class="wrap6">
            <div class="ico-wrap">
              <span class="mbr-iconfont fa-tablet fa"></span>
            </div>
            <div class="text-wrap vcenter">
              <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5">Responsive Design </h2>
              <p class="mbr-fonts-style text1 mbr-text display-6">Responsive design for the website is prime requirement of almost everyor individual willing to have a website for their business. Understanding the same, our
                professionals have deeply analysed the factors which help in developing a perfectly
                responsive website. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="whatwedo container slideanim">
      <h3 data-delighter class="right">What We Do</h3>
      <div class="row" style="width: 100%;">
        <ul data-delighter>
          <p>In the fast track IT industry, TechZapp believes in sticking to the basics to deliver the best results
            for our clientele. With plenty of experience of the market, our professionals follow the fundamental
            path of firstly understanding the needs of our customers and only then proceeding with the project.
            For the same, we have set up a simple yet effective mechanism which surely would impress you.
          </p>
          <p>Firstly, our sales team representative would approach you to seal the deal after coming across your
            offer. Post getting the contract, our development team would connect with you and will send you a
            questionire covering everything in regard to the website or mobile development. Your answers
            would help us understand your needs and desires perfectly which is a very important factor when it
            comes to offering IT services. </p>
          <p>Post collecting all the info from your side, we prepare a template website and forward it back to
            you for review. If you seek any changes, we do them and reconfirm it with you to move on to the
            next phase of the development.</p>
          <p>We never shyaway from covering the extra mile for our customers. We are available 24*7 to solve
            your issues and queries. After fulfilling all your requirements, we also offer some expert advice on
            the basis of the current trends in your industry. </p>
          <p>Apart from delivering best quality services, we have also maintained a reputation of never missing
            the deadline for delivering any milestone of the projects. During the past few years, we have
            established few very strong bonds with our clients which give us confidence of keep moving ahead
            in the industry.</p>
        </ul>
      </div>
    </div>
  </section>
  <section class="skills slideanim">
    <h2>Our Skills</h2>
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="tile-progress tile-primary">
            <div class="tile-header">
              <h3>Web Design</h3>
            </div>
            <div class="tile-progressbar">
              <span data-fill="80%" style="width: 80%;"></span>
            </div>
            <div class="tile-footer">
              <h4>
                <span class="pct-counter">80</span>%
              </h4>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="tile-progress tile-red">
            <div class="tile-header">
              <h3>Web Devlopment</h3>
            </div>
            <div class="tile-progressbar">
              <span data-fill="99%" style="width:99%;"></span>
            </div>
            <div class="tile-footer">
              <h4>
                <span class="pct-counter">99</span>%
              </h4>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="tile-progress tile-blue">
            <div class="tile-header">
              <h3>App Design</h3>
            </div>
            <div class="tile-progressbar">
              <span data-fill="78%" style="width: 78%;"></span>
            </div>
            <div class="tile-footer">
              <h4>
                <span class="pct-counter">78</span>%
              </h4>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="tile-progress tile-aqua">
            <div class="tile-header">
              <h3>App Development</h3>
            </div>
            <div class="tile-progressbar">
              <span data-fill="95%" style="width: 95%;"></span>
            </div>
            <div class="tile-footer">
              <h4>
                <span class="pct-counter">95</span>%
              </h4>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3">
          <div class="tile-progress tile-green">
            <div class="tile-header">
              <h3>Visitors</h3>
            </div>
            <div class="tile-progressbar">
              <span data-fill="94%" style="width: 94%;"></span>
            </div>
            <div class="tile-footer">
              <h4>
                <span class="pct-counter">94</span>%
              </h4>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="tile-progress tile-cyan">
            <div class="tile-header">
              <h3>Branding</h3>
            </div>
            <div class="tile-progressbar">
              <span data-fill="70%" style="width: 70%;"></span>
            </div>
            <div class="tile-footer">
              <h4>
                <span class="pct-counter">70</span>%
              </h4>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="tile-progress tile-purple">
            <div class="tile-header">
              <h3>Marketing</h3>
            </div>
            <div class="tile-progressbar">
              <span data-fill="70%" style="width: 70%;"></span>
            </div>
            <div class="tile-footer">
              <h4>
                <span class="pct-counter">70</span>%
              </h4>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="tile-progress tile-pink">
            <div class="tile-header">
              <h3>Photoshop</h3>
            </div>
            <div class="tile-progressbar">
              <span data-fill="85" style="width: 85%;"></span>
            </div>
            <div class="tile-footer">
              <h4>
                <span class="pct-counter">85</span>%
              </h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div id="particles-js">
      <div class="col-lg-12 text">
        <h1>Innovative Ideas + Awesome Design</h1><br><br>
        <a href="#contact">Get a Quote</a>
      </div>
    </div>
  <section class="slideanim">
    <div class="strategy container">
      <h3>Strategy We Attain</h3>
      <p>We approach every phase of the product development that adds value and transform every spectrum of your
        business idea & tends you to excel towards your ambitions.</p>
      <div class="col-lg-12">
        <?php echo $this->Html->image('process.png', array('alt' => 'processimg', 'width' => '100%')) ?>
      </div>
    </div>

  </section>
  <!--==========================
Facts Section
============================-->
  <section id="facts" class="wow fadeIn slideanim">
    <div class="container">
      <header class="section-header">
        <h3>Facts</h3>
      </header>
      <div class="row counters">
        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">120</span>
          <p>Happy Clients</p>
        </div>
        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">70</span>
          <p>Projects</p>
        </div>
        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">1,364</span>
          <p>Tweets</p>
        </div>
        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">30</span>
          <p>Meetings</p>
        </div>
      </div>
      <div class="facts-img">
        <?php echo $this->Html->image('facts-img.png', array('alt' => 'factimg', 'class' => 'img-fluid')) ?>
      </div>
    </div>
  </section><!-- #facts -->
  <!--==========================
Portfolio Section
============================-->
  <section id="portfolio">
    <div class="container">
      <header class="section-header">
        <h3 class="section-title">Our Stunning Work</h3>
      </header>

      <div class="row">
        <div class="col-lg-4 col-xs-6 hover-zoomin">
          <a href="#" title="">
            <?php echo $this->Html->image('portfolio/localspot.webp', array('alt' => 'dontworry', 'class' => 'img-thumbnail', 'width' => '100%')) ?>
          </a>
        </div>

        <div class="col-lg-4 col-xs-6 hover-zoomin">
          <a href="#" title="">
            <?php echo $this->Html->image('portfolio/dontworry.webp', array('alt' => 'dontworry', 'class' => 'img-thumbnail', 'width' => '100%')) ?>
          </a>
        </div>

        <div class="col-lg-4 col-xs-6 hover-zoomin">
          <a href="#" title="">
            <?php echo $this->Html->image('portfolio/global.webp', array('alt' => 'dontworry', 'class' => 'img-thumbnail', 'width' => '100%')) ?>
          </a>
        </div>
        
      </div>

      <div class="row" style="margin-top:30px;">
        <div class="col-lg-4 col-xs-6 hover-zoomin">
          <a href="#" title="">
            <?php echo $this->Html->image('portfolio/krubpark2.webp', array('alt' => 'dontworry', 'class' => 'img-thumbnail', 'width' => '100%')) ?>
          </a>
        </div>

        <div class="col-lg-4 col-xs-6 hover-zoomin">
          <a href="#" title="">
            <?php echo $this->Html->image('portfolio/dontworry2.webp', array('alt' => 'dontworry', 'class' => 'img-thumbnail', 'width' => '100%')) ?>
          </a>
        </div>

        <div class="col-lg-4 col-xs-6 hover-zoomin">
          <a href="#" title="">
            <?php echo $this->Html->image('portfolio/krubpark2.webp', array('alt' => 'dontworry', 'class' => 'img-thumbnail', 'width' => '100%')) ?>
          </a>
        </div>
        
      </div>



    </div>
  </section>
  <!-- #portfolio -->

  <section class="contact slideanim " id="contact">
    <div class="container" style="padding-top: 20px;padding-bottom: 20px;">
      <div class="row">
        <div class="col-md-12">
          <h4>Get In Touch</h4>
          <hr>
        </div>
        <div class="col-md-6">
          <div class="address">
            <h5>Address:</h5>
            <ul class="list-unstyled">
              <li>Amrapali Tower</li>
              <li> Indraprasth Colony,</li>
              <li>Dehradun, Uttrakhand</li>
            </ul>
          </div>
          <div class="email">
            <h5>Email:</h5>
            <ul class="list-unstyled">
              <li>info@techzapp.com</li>
            </ul>
          </div>
          <div class="phone">
            <h5>Phone:</h5>
            <ul class="list-unstyled">
              <li> +91- 7417004005</li>
            </ul>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-body">
              <?php echo $this->Session->flash('positive') ?>
              <?php echo $this->Form->create('Contact', array('url' => array('controller' => 'Users', 'action' => 'index'))); ?>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <?php echo $this->Form->input('name', array('type' => 'text',  'class' => 'form-control',  'div' => false, 'label' => 'Name<span class="required">*</span>')); ?>
                </div>
                <div class="form-group col-md-6">
                  <?php echo $this->Form->input('email', array('type' => 'text',  'class' => 'form-control', 'id' => 'email', 'div' => false, 'label' => 'Email<span class="required">*</span>')); ?>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <?php echo $this->Form->input('phone', array('type' => 'text',  'class' => 'form-control', 'id' => 'phone', 'div' => false, 'maxlength' => '10', 'label' => 'Phone(+91)<span class="required">*</span>')); ?>
                </div>
                <div class="form-group col-md-12">
                  <?php echo $this->Form->input('message', array('type' => 'textarea', 'cols' => '40', 'rows' => '5',  'class' => 'form-control', 'id' => 'message', 'div' => false, 'label' => 'Message<span class="required">*</span>')); ?>
                </div>
                <p>We need the contact information you provide to us to give you the information
                  you requested about our services.
                </p>
              </div>
              <div class="form-row">
                <?Php echo $this->Form->button('Send', array('type' => 'submit', 'class' => 'btn btn-danger')); ?>
              </div>
              <?php echo $this->Form->end(); ?>
            </div>
          </div>
        </div>
        <hr>
      </div>
    </div>
  </section>

</main>
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1561436720/particles.js"></script>
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1561436735/app.js"></script>
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<script>
  $(document).ready(function() {
    var count_particles, stats, update;
    stats = new Stats;
    stats.setMode(0);
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';
    document.body.appendChild(stats.domElement);
    count_particles = document.querySelector('.js-count-particles');
    update = function() {
      stats.begin();
      stats.end();
      if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
        count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
      }
      requestAnimationFrame(update);
    };
    requestAnimationFrame(update);
  });


  $("#ContactIndexForm").validate({
    rules: {
      "data[Contact][name]": {
        required: true,
        maxlength: 20,
      },
      "data[Contact][email]": {
        required: true,
        email: true,
       
      },
      "data[Contact][phone]": {
        required: true,
        digits: true,
        minlength: 10,
        maxlength: 10,
      },
      "data[Contact][message]": {
        required: true,
      },
    },
    messages: {
      "data[Contact][name]": {
        required: "Please enter  name.",
        minlength: 'Minimum length should be greater than 25 characters',
      },
      "data[Contact][email]": {
        required: "Please enter email.",
        email: "Please enter valid email.",
      
      },
      "data[Contact][phone]": {
        required: "Please enter phone number.",
        digits: 'Please use only digits',
        minlength: 'Minlength must be 10 digits',
        maxlength: 'Maxlength must be 10 digits',
      },
      "data[Contact][message]": {
        required: "Please enter your message.",
      },
    }
  });

  $('.menu, .overlay').click(function() {
    $('.menu').toggleClass('clicked');

    $('#nav').toggleClass('show');

  });
</script>
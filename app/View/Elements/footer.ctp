    <!--==========================
Footer
============================-->
<footer id="footer">
      <div class="footer-top">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 footer-info">
              <h3>Techzapp Solutions</h3>
              <p>We mainly work on Open-Source Software (OS) which surely is the most gigantic industry in today’s tech
                age. Our team of developers have exceptional knowledge and experience of working on the technology and
                since our establishment, we have delivered some memorable projects for our domestic and international
                client.</p>
            </div>
            <div class="col-lg-4 col-md-4 footer-links">
              <h4>Useful Links</h4>
              <ul>
              <li><i class="ion-ios-arrow-right"></i> <?php echo $this->Html->link('Careers', array('controller' => 'Careers', 'action' => 'index')); ?></li>
                <li><i class="ion-ios-arrow-right"></i><?php echo $this->Html->link('Terms of service', array('controller' => 'conditions', 'action' => 'index')); ?></li>
                <li><i class="ion-ios-arrow-right"></i> <?php echo $this->Html->link('Privacy policy', array('controller' => 'conditions', 'action' => 'index')); ?></li>
                <li><i class="ion-ios-arrow-right"></i> <?php echo $this->Html->link('Internship/Training', array('controller' => 'trainings', 'action' => 'index')); ?></li>
              </ul>
            </div>
            <div class="col-lg-4 col-md-4 footer-contact">
              <h4>Contact Us</h4>
              <p>
                Amrapali Tower<br>
                Indraprasth Colony<br>
                Dehradun, Uttrakhand <br>
                <strong>Phone:</strong>+91- 7417004005 <br>
                <strong>Email:</strong> info@techzapp.com<br>
              </p>
              <div class="social-links">
                
                <a href="https://twitter.com/techzapps" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="https://www.facebook.com/Techzapp-Solutions-Pvt-Ltd-1030948153670157" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="https://www.linkedin.com/company/11217621" class="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
                
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="copyright">
        <p>&copy; <?php echo date('Y'); ?> <a href="http://www.Techzapp.com" target="_blank">Techzapp Solutions</a></p>
        </div>
      </div>
    </footer><!-- #footer -->
<nav class="navbar navbar-expand-md bg-light navbar-light fixed-top ">
  <!-- Brand -->
  <a class="navbar-brand" href="https://techzapp.com/"><img src="img/logo.webp" width="150px"></a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="nav navbar-nav  ml-auto">
      <li class="nav-item nav-link">
    
        <?php echo $this->Html->link('Home', array('controller' => 'users', 'action' => 'index')); ?>
      </li>
     
    </ul>
  </div>
</nav>
<style>.bg-light {
    background-color: #fff!important;
}
</style>
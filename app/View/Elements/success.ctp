<div id="<?php echo h($key) ?>" class="flash-message-success">
   <?php echo ucwords($message); ?>
  
</div>
<style type="text/css">
  div.flash-message-success {
    width: 100%;
    color: #fff;
    background: #e91e63;
    margin-left: 0px;
    font-size: 17px;
    padding: 15px;
    text-align: center;
    margin-bottom: 20px;
  }
</style>
<script type="text/javascript">
  $(document).ready(function() {
    $(".flash-message-success").delay(4000).slideUp(500);
  });
</script>
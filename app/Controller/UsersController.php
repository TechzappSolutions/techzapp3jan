<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController
{

	/**
	 * This controller does not use a model
	 *
	 * @var array
	 */
	public $uses = array('Contact');
	public $components = array('Email', 'Session', 'Cookie');
	public $helpers = array('Cache', 'Html', 'Session', 'Form', 'Flash');

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 * @return void
	 * @throws NotFoundException When the view file could not be found
	 *	or MissingViewException in debug mode.
	 */
	public function index()
	{
		$this->layout = 'techzapp';
		$this->set('title', "Techzapp Solutions Software Development");
		if ($this->request->is('Post')) {
			if (!empty($this->request->data)) {
				if ($this->Contact->Save($this->request->data)) {
					$to = 'info@techzapp.com';
					$subject = 'Techzapp Site Contactus Email';
					$message = $this->request->data;
					if ($this->sendmail($to, $subject, $message)) {

						$this->Session->setFlash('Email sent.', 'success', array(
							'key' => 'positive'
						), 'positive');

						return $this->redirect('/users/index/#contact');
					}
				} else {
					return $this->redirect('/users/index/#contact');
				}
			}
		}
	}
	public function sendmail($to = null, $subject = '', $messages = null, $ccParam = null, $from = null, $reply = null, $path = null, $file_name = null)
	{
		$this->layout = false;
		$this->render(false);
		$name = $messages['Contact']['name'];
		$email = $messages['Contact']['email'];
		$phone = $messages['Contact']['phone'];
		$message = nl2br($messages['Contact']['message']);
		$Email = new CakeEmail();
		$Email->config('smtp');
		$Email->viewVars(array('name' => $name, 'email' => $email,  'phone' => $phone, 'message' => $message));
		$Email->template('comman_email_template', 'comman_email_template');
		return $Email->emailFormat('html')
			->from(array('no-reply@techzapp.com' => 'no-reply@techzapp.com'))
			->to($to)
			->subject($subject)
			->send();
	}
}

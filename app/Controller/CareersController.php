<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class CareersController extends AppController
{

	/**
	 * This controller does not use a model
	 *
	 * @var array
	 */
	public $uses = array('Job', 'Job');
	public $components = array('Email', 'Session', 'Cookie');
	public $helpers = array('Cache', 'Html', 'Session', 'Form', 'Flash');

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 * @return void
	 * @throws NotFoundException When the view file could not be found
	 *	or MissingViewException in debug mode.
	 */
	public function index()
	{
		$this->layout = 'otherpages';
		$this->set('title', "Techzapp Solutions Software Development");
	}
	public function jobapply()
	{
		$this->autoRender = false;
		if ($this->request->is('post')) {
			//  PR($this->request->data);die;
			if (!empty($this->request->data)) {
				if (!empty($this->request->data['Job']['resume']['tmp_name']) && is_uploaded_file($this->request->data['Job']['resume']['tmp_name'])) {
					$fileName = basename(strtotime("now") ."_".$this->request->data['Job']['resume']['name']);
					$filetype = basename($this->request->data['Job']['resume']['type']);
					$uploadPath = WWW_ROOT . 'img/upload/';
					$uploadFile = $uploadPath . $fileName;
					// pr($uploadFile);die;
					$ext = explode('.', $fileName);
					$allowExtension = array('doc' => 'doc', 'docx' => 'docx', 'pdf' => 'pdf');
					if (in_array($ext[1], $allowExtension)) {
						if (move_uploaded_file($this->request->data['Job']['resume']['tmp_name'], $uploadFile)) {
							$this->request->data['Job']['resume'] = '/img/upload/' . $fileName;
							//    pr( $this->request->data['Job']['resume']);die;
							if ($this->Job->Save($this->request->data)) {
								$to = 'info@techzapp.com';
								$subject = 'Techzapp Site Job Regarding Email';
								$message = $this->request->data;
								$message["Job"]["resume"] = $fileName;
								// PR($message);die;
								if ($this->sendmail($to, $subject, $message)) {
									$this->Session->setFlash('Email sent.', 'success', array(
										'key' => 'positive'
									), 'positive');
									return $this->redirect(array('controller' => 'careers', 'action' => 'index'));
								}
							} else {
								echo "wrong";
								die;
								return $this->redirect(array('controller' => 'careers', 'action' => 'index'));
							}
						}
					} else {
						$this->Flash->successNotification('Failed.', array(
							'key' => 'positive',
						));
					}
				}
			} else {
				/*upload images end*/
				unset($this->request->data['Job']['resume']);
			}
		}
	}
	public function sendmail($to = null, $subject = '', $messages = null, $ccParam = null, $from = null, $reply = null, $path = null, $file_name = null)
	{
		$this->layout = false;
		$this->render(false);
		$fullname = $messages['Job']['fullname'];
		$jobposition = $messages['Job']['job_position'];
		$resume = $messages['Job']['resume'];
		$message = nl2br($messages['Job']['message']);
		$Email = new CakeEmail();
		$Email->filePaths  = array('/img/upload/');
		$Email->attachments(array(1 =>  WWW_ROOT . 'img'  . DS . 'upload' . DS . $resume));
		// debug($messages['Job']['resume']);die;
		$Email->config('smtp');
		$Email->viewVars(array('fullname' => $fullname, 'resume' => $resume, 'job_position' => $jobposition, 'message' => $message));
		$Email->template('job_email_template', 'job_email_template');
		return $Email->emailFormat('html')
			->from(array('no-reply@techzapp.com' => 'no-reply@techzapp.com'))
			->to($to)
			->subject($subject)
			->send();
	}
}
